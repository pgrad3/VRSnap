﻿interface IMover {
	void getMoving ();
	void startMoving ();
	void stopMoving ();
}
